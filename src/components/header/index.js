import React from 'react';
import { TouchableOpacity, Image, View, Text,StyleSheet } from 'react-native';
import { DrawerActions, withNavigation } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';

class LogoTitle extends React.Component {
  constructor(props, firtScreen) {
    super(props);
    this.state = {
      firtScreen: true
    }
  }
  changeTab(index) {
    switch (index) {
      case 0:
        this.setState({ firtScreen: true });
        break;
      case 1:
        this.setState({ firtScreen: false });
        break;
      default:
        break;
    }
  }

  toggleDrawer() {
    this.props.navigation.dispatch(DrawerActions.toggleDrawer())
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.toggleDrawer()} style={{ width: 60, padding: 15 }}>
          <Ionicons name='md-menu' size={24} color='black' />
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', flex: 1, height: '100%' }}>
          <TouchableOpacity onPress={() => this.props.tabGelato}
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D3D3D3', }}>
            <Image
              source={this.props.logo1}
              style={{ width: 40, height: 60 }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.tabCiaoLink}
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              source={this.props.logo2}
              style={{ width: 50, height: 60 }}
            />
          </TouchableOpacity>
          <View style={{ width: 60 }}>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1, height: '100%', 
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'center',
     alignItems: 'center',
      borderBottomColor: 'grey',
       borderBottomWidth: 1 
  }
})
export default withNavigation(LogoTitle);