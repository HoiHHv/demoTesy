import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import LogoTitle from '../../components/header/index'
import HeaderTest from '../../components/headerTest'
let img1 = require('../../assets/img/logo.png')
class HistoryScreeen extends Component {
  constructor(props) {
    super(props)
  }
  static navigationOptions = {
    title: 'Lịch sử',
  }
  render() {
    return (
      <View style={{ flex: 1,  }}>
        <View style ={{height:80}}>
          <HeaderTest logo1 ={img1} />
        </View>
        <View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
          <Text>HistoryScreeen</Text> 
        </View>   
      </View>
    )
  }
}
export default HistoryScreeen;